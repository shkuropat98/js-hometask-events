'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

const menuButton = document.getElementById(`menuBtn`);
const menu = document.getElementById(`menu`);

menuButton.addEventListener(`click`, () => {
  menu.classList.toggle(`menu-opened`);
})
menu.addEventListener(`click`, () => {
  menu.classList.remove(`menu-opened`);
})
/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/


const field = document.getElementById(`field`);
const movedBlock = document.getElementById(`movedBlock`);

field.addEventListener(`click`, (event) => {
  const fieldCoord = field.getBoundingClientRect();
  const blockCoord = movedBlock.getBoundingClientRect()

  const clickX = event.clientX - fieldCoord.x ;
  const fieldWidthMax = fieldCoord.width - blockCoord.width;

  const clickY = event.clientY - fieldCoord.y;
  const fieldHeightMax = fieldCoord.height - blockCoord.height;

  if (event.target.getAttribute(`id`) !== movedBlock.id) {

    if (clickX <= fieldWidthMax && clickY <= fieldHeightMax)  {
      movedBlock.style.top = `${clickY}px`;
      movedBlock.style.left = `${clickX}px`;
    }
    if (clickX > fieldWidthMax)  {
      movedBlock.style.top = `${clickY}px`;
      movedBlock.style.left = `${fieldWidthMax}px`;
    }
    if (clickY > fieldHeightMax)  {
      movedBlock.style.top = `${fieldHeightMax}px`;
      movedBlock.style.left = `${clickX}px`;
    }
    if (clickY >= fieldHeightMax && clickX >= fieldWidthMax)  {
      movedBlock.style.top = `${fieldHeightMax}px`;
      movedBlock.style.left = `${fieldWidthMax}px`;
    }
  }
});

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
const messager = document.getElementById(`messager`);
messager.addEventListener(`click`, (event) => {
  console.log(event.currentTarget)
  if (event.target.classList.contains(`remove`) ) {        
    event.target.parentNode.remove();    
  }
})


/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/

const linksList = document.getElementById(`links`)

linksList.addEventListener(`click`, (event)=> {
      
  if (event.target.tagName ===`A`) {
    event.preventDefault()
    const linkTo = event.target.href
    const confirmResult = confirm(`Действительно хочешь уйти?`)
    if (confirmResult) {
      window.location.assign(linkTo)
    }
  }
})

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/

const fieldHeader = document.getElementById(`fieldHeader`);
const taskHeader = document.getElementById(`taskHeader`)
fieldHeader.addEventListener(`input`, (e) => {
  taskHeader.textContent = e.target.value;
}) 
